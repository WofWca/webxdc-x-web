//@ts-check
import { Peer, util } from "peerjs";

// Not inlining these so make the scrapers' work harder, idk
const myServerUser = "efWGF1PWGWU9VE0EE2";
const myServerPass = "uyaegRXemW3jfPLa";
/**
 * @type {import("peerjs").PeerJSOption}
 */
const peerOptions = {
  // TODO self-host PeerJS server, or make this configurable?
  debug: 2,
  config: {
    iceServers: [
      // Currently PeerJS provides a free TURN server.
      ...util.defaultConfig.iceServers,
      // My personal TURN server with 1000 GB monthly limit
      // (please don't abuse. Just go to https://expressturn.com and they'll
      // give you a free one too).
      {
        urls: "turn:relay1.expressturn.com:3478",
        username: myServerUser,
        credential: myServerPass,
      },
    ],
  }
};

let roomName = location.hash.slice(1);
if (!roomName) {
  location.hash = roomName = crypto.randomUUID();
}

window.Peer = Peer;


const peerP = registerOnSignalingServer();

// TODO perf: close all the connections on page close? Is it better, or is
// automatic cleanup good enough?
/** @type {import("peerjs").DataConnection[]} */
let dataConnections = [];
peerP.then(peer => {
  connectToPreexistingPeers(peer, onNewDataConnection);
});
peerP.then(peer => {
  peer.on("connection", onNewDataConnection);
});
/**
 * @param {import("peerjs").DataConnection} dataConnection
 */
function onNewDataConnection(dataConnection) {
  console.log("New DataConnection", dataConnection);
  const onOpen = () => {
    dataConnections.push(dataConnection);
    dataConnection.on("data", handleWebxdcUpdate);
    for (const update of sentUpdates) {
      dataConnection.send(update);
    }
  };
  if (dataConnection.open) {
    onOpen();
  } else {
    dataConnection.once("open", onOpen);
  }
}

let nextSerial = 1;
// function onData(dataConnection, data) {
function handleWebxdcUpdate(data) {
  const serial = nextSerial;
  nextSerial += 1;

  const update = {
    ...data,
    serial: serial,
    max_serial: serial,
    // These are sent by the peer, as well as payload.
    // info, document, summary
  };
  console.log("new webxdc update", update)
  webxdcUpdateListener(update);
}

/** @type {WebxdcUpdate[]} */
const sentUpdates = [];
/** @type {(update: WebxdcUpdate) => void} */
let webxdcUpdateListener = () => {};
// Yes, this changes on page reloads. We don't handle reloads in this prototype
// currently.
const selfAddr = crypto.randomUUID();
globalThis.webxdc = {
  setUpdateListener(newListener, serial) {
    // TODO handle `serial`.
    webxdcUpdateListener = newListener;
    // TODO replay all received updates on `newListener`.
    // though most apps would most likely call `setUpdateListener` before
    // we've managed to establish the connection.
  },
  sendUpdate(update, descr) {
    for (const connection of dataConnections) {
      connection.send(update);
    }
    handleWebxdcUpdate(update);
    sentUpdates.push(update);
  },
  selfAddr,
  selfName: selfAddr,
  // TODO `importFiles`, `sendToChat`? `sendToChat` = just download the file?
}

/**
 * @param {string} roomName
 * @param {number} n
 * @returns
 */
function getNthPeerId(roomName, n) {
  // return `${roomName}-${n}`;
  return roomName + '-' + n;
}

/**
 * @param {import("peerjs").Peer} localPeerObj
 */
async function connectToPreexistingPeers(localPeerObj, onNewDataConnection) {
  console.log("connecting to peers");
  // Keep in mind that there could be gaps in the room because some peers
  // might have disconnected.
  // TODO But yeah, we don't handle reconnections properly, because currently
  // a reconnected peer won't get the messages that it sent in a previous
  // session.
  // Perhaps we should save them to the browser storage.
  const connectionPromises = [];
  const NUM_MAX_PEERS = 20;
  for (let peerInd = 0; peerInd < NUM_MAX_PEERS; peerInd++) {
    const peerId = getNthPeerId(roomName, peerInd);
    if (peerId === localPeerObj.id) {
      continue;
    }
    const p = tryConnectToPeer(localPeerObj, peerId).then(dataConnection => {
      if (!dataConnection) {
        console.log(
          "Failed to connect to",
          getNthPeerId(roomName, peerInd),
          "most likely there is no such peer"
        );
        return;
        // return null;
      }
      console.log("Connected to", dataConnection.peer);
      onNewDataConnection(dataConnection);
      // return dataConnection;
    });
    connectionPromises.push(p);
  }
  // return (await Promise.allSettled(connectionPromises))
  //   .filter(res => res.status === 'fulfilled')
  //   .map(res => res.value);
};

async function registerOnSignalingServer() {
  for (let peerInd = 0; ; peerInd++) {
  // for (let peerInd = Math.round(Math.random()); ; peerInd++) {


    // TODO fix: ok, apparently if you try to do `new Peer()` with the same
    // ID twice from the same machine, it will forget the first one and allow
    // to register the second one. Big bruh moment.
    //
    // We probably need another way of checking whether an id is available.
    // That is, perhaps, with `connect`, or maybe some raw-er request.
    //
    // Or, perhaps, a completely different way of working with rooms.
    //
    // If you uncomment this (and add `localStorage.other` on one of the
    // browsers), it starts working.
    if (peerInd === 0 && localStorage.getItem('other')) {
      continue;
    }


    // Try until we get an available ID.
    const peer = await tryRegisterOnSignalingServerWithId(
      getNthPeerId(roomName, peerInd)
    );
    if (peer) {
      console.log("Registered on signaling server as", peer.id);
      return peer;
    }
    console.log("Failed to register on signaling server as", getNthPeerId(roomName, peerInd));
  }
}
/**
 * @returns {Promise<undefined | Peer>}
 */
async function tryRegisterOnSignalingServerWithId(peerId) {
  const peer = new Peer(peerId, peerOptions);

  window.peer = peer;


  console.log("Created peer obj");
  return new Promise(r => {
    peer.on("error", err => r(undefined));
    peer.on("open", () => {
      console.log("open");
      r(peer)
    });
  });
}
window.tryRegisterOnSignalingServerWithId = tryRegisterOnSignalingServerWithId;
/**
 * Resolves with `DataConnection` when the connection is open.
 * @param {Peer} localPeerObj
 * @param {Parameters<Peer['connect']>[0]} peerId
 * @returns {Promise<undefined | import("peerjs").DataConnection>}
 */
async function tryConnectToPeer(localPeerObj, peerId) {
  console.log("Trying to connect to", peerId);
  const dataConnection = localPeerObj.connect(peerId, {
    reliable: true,
    // TODO perf: Maybe use this, since webxdc doesn't support things like
    // `Blob` anyway? Though it might in the future.
    // serialization: 'json'
  });
  return new Promise(r => {
    // TODO are you sure that the error happens here if the peer with this ID
    // does not exist?
    // dataConnection.on("error", err => r(undefined));

    // Keep in mind that this (I think) overrides the previously set "error"
    // listener
    // dataConnection.on("error", err => {
    //   console.log("DATA ERROR");
    // })
    localPeerObj.on("error", err => {
      // This is stupid. See https://github.com/peers/peerjs/issues/924
      // We're relying on this string:
      // https://github.com/peers/peerjs/blob/633cd84929f398899a06e8281706e3040410d706/lib/peer.ts#L393
      if (err.message.endsWith(peerId)) {
        r(undefined);
      }
    })
    // TODO remove event listeners with `off`.
    dataConnection.on("error", () => r(undefined));
    // TODO fix: doesn't this mean that we might miss some messages?
    // I think we're supposed to do `.on("data")` synchronously inside the
    // "open" callback?
    dataConnection.on("open", () => r(dataConnection));
  });
}
